<!DOCTYPE html>
<html lang="vi">
<head>
    <!--    --><?php //include_once("./Model/connect.php");?>

    <?php include_once("./Controller/StudentController.php");
    $student = mysqli_fetch_array($C_Students);
    ?>


    <?php require_once "layout/header.php"; ?>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php require_once "layout/navbar.php"; ?>

        <!-- top tiles -->
        <!-- nhan vien -->
        <div class="right_col" role="main" style="min-height: 1552px;">
            <!--Form add data teams-->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cập nhật học viên</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form class="form-horizontal form-label-left" novalidate action="./Controller/StudentController.php?id=<?php echo $_GET['id'] ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="full_name" id="first-name" required="required"
                                       class="form-control col-md-7 col-xs-12"
                                       value="<?php echo $student['full_name'] ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Team_id
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select name="class_id" id="" class="form-control col-md-7 col-xs-12">
                                        <?php
                                        include_once("./Controller/ClassController.php");
                                        while ($row = mysqli_fetch_array($C_Class)) {
                                            ?>
                                            <option value="<?php echo $row['id']; ?>" <?php if($student['class_id']==$row['id']) echo "selected"  ?>><?php echo $row['class_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" name="submit" class="btn btn-success">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                    <!--                    Xu ly submit-->

                </div>
            <!-- footer content -->
            <?php require_once "layout/footer.php"; ?>
            <!-- end footer content -->
</body>
</html>