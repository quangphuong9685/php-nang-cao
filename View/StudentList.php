<!DOCTYPE html>
<html lang="vi">
<head>
    <?php require_once "layout/header.php"; ?>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php require_once "layout/navbar.php"; ?>
        <div class="right_col" role="main">
            <!-- nhan vien -->
            <div class="col-md-12 col-sm-12 col-xs-12" id="users">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Danh sách học viên
<!--                            <small>View</small>-->
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <button type="button" name="btn-add" class="btn btn-info"><a href="?type=create">Thêm mới</a></button>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Lớp</th>
                                <th>Thao_tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                include_once("./Controller/StudentController.php");

                                while ($row = mysqli_fetch_array($C_Students)) {
                                ?>
                            <tr>
                                <td><?php echo $row['id'] ?></td>
                                <td><?php echo $row['full_name'] ?></td>
                                <td><?php echo $row['class_name'] ?></td>
                                <td>
                                    <a href="?id=<?php echo $row["id"] ?>&type=edit" class="btn btn-info btn-xs"><i
                                                class="fa fa-pencil"></i> Cập nhật</a>
                                    <a href="?id=<?php echo $row["id"] ?>&type=delete"
                                       class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Xóa </a>
                                </td>
                            </tr>
                            <?php } ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end-nhanvien -->
        </div>
        <!-- footer content -->
        <?php require_once "layout/footer.php"; ?>
        <!-- end footer content -->
</body>
</html>