<?php
/**
 * Created by PhpStorm.
 * User: quangqa
 * Date: 2022-08-12
 * Time: 14:35
 */
include_once("connect.php");

class ModelStudent extends MakeConnection
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllStudent()
    {
        $sql = 'SELECT students.*,class.class_name FROM `students` JOIN `class` ON students.class_id=class.id';
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    public function getStudentDetail($id)
    {
        $sql = 'SELECT * FROM `students` where `id`=' . $id;
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    public function updateStudent($request)
    {
        $sql = sprintf("UPDATE `students` SET `full_name` = '%s', `class_id` =%d WHERE `students`.`id` =%d", $request['full_name'], (int)$request['class_id'], $_GET['id']);
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    public function createStudent($request)
    {
        $sql = sprintf("INSERT INTO `students` (`full_name`,`class_id`) VALUES ('%s',%b)", $request['full_name'], (int)$request['class_id']);
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    public function destroyStudent($id)
    {
        $sql = sprintf("DELETE FROM `students` WHERE `students`.`id` =%d", $id);
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }
}