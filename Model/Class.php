<?php
/**
 * Created by PhpStorm.
 * User: quangqa
 * Date: 2022-08-12
 * Time: 14:35
 */
include_once("connect.php");

class ModelClass extends MakeConnection
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Danh sách nhân viên
     *
     * @return array
     */
    public function getAll()
    {
        $sql = 'SELECT * FROM `class`';
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    /**
     * Chi tiết học sinh
     *
     * @param $id
     * @return mixed
     */
    public function getDetail($id)
    {
        $sql =   'SELECT * FROM `class` where `id`='.$id;
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }

    public function create($id)
    {
        $sql = 'SELECT students.*,class.class_name FROM `students` JOIN `class` ON students.class_id=class.id';
        $result = mysqli_query($this->conn, $sql);
        return $result;
    }
}