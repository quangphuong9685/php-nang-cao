<?php
/**
 * Created by PhpStorm.
 * User: quangqa
 * Date: 2022-08-12
 * Time: 14:39
 */
$base_path = __DIR__ . '/../';
require_once $base_path . "/Model/Student.php";

class StudentController
{
    public $modelGlobal;

    public function __construct()
    {
        $this->modelGlobal = new ModelStudent();// khởi tạo model
    }

    /**
     * Hiển thị danh sách học sinh
     *
     * @return array
     */
    public function index()
    {
        $students = $this->modelGlobal->getAllStudent();
        return $students;
    }

    public function store($request)
    {
        $this->modelGlobal->createStudent($request);
        header('Location: ' . '/'); // chuyển hướng trang sau khi xử lý xong
    }

    /**
     * Hiển thị chi tiết học sinh
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $students = $this->modelGlobal->getStudentDetail($id);
        return $students;
    }

    /**
     * Cập nhật danh sách học sinh
     *
     * @param $request
     */
    public function update($request)
    {
        $this->modelGlobal->updateStudent($request);
        header('Location: ' . '/'); // chuyển hướng trang sau khi xử lý xong
    }

    /**
     * Xóa học sinh
     *
     * @param $id
     */
    public function destroy($id)
    {
        $this->modelGlobal->destroyStudent($id);
        header('Location: ' . '/'); // chuyển hướng trang sau khi xử lý xong
    }
}


$C_Students = new StudentController();
if (isset($_POST['submit']) && $_POST['_method'] =='PUT') {
    $C_Students->update($_POST);
}elseif (isset($_POST['submit']) && $_POST['_method'] =='POST') {
    $C_Students->store($_POST);
} elseif ($_GET['id'] && $_GET['type'] == 'edit') {
    $C_Students = $C_Students->edit($_GET['id']);
} elseif ($_GET['id'] && $_GET['type'] == 'delete') {
    $C_Students->destroy($_GET['id']);
} else {
    $C_Students = $C_Students->index();
}

return $C_Students;
