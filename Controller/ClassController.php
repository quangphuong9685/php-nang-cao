<?php
/**
 * Created by PhpStorm.
 * User: quangqa
 * Date: 2022-08-12
 * Time: 14:39
 */

$base_path = __DIR__ . '/../';
require_once $base_path."/Model/Class.php";

class ClassController
{
    public function index()
    {
        $model_class = new ModelClass();// khởi tạo model
        $class = $model_class->getAll();
        return $class;
    }

}

$C_Class = new ClassController();
$C_Class = $C_Class->index();
return $C_Class;
